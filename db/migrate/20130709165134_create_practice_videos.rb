class CreatePracticeVideos < ActiveRecord::Migration
  def change
    create_table :practice_videos do |t|
      t.integer :training_id
      t.integer :practice_id
      t.string :video_link

      t.timestamps
    end
  end
end
