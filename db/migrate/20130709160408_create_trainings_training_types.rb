class CreateTrainingsTrainingTypes < ActiveRecord::Migration
  def change
    create_table :trainings_training_types do |t|
      t.integer :training_id
      t.integer :training_type_id
    end
  end
end
