class RenamePracticeVideo < ActiveRecord::Migration
  def self.up
    rename_table :practice_videos, :exercise_videos 
  end

  def self.down
	rename_table :exercise_videos, :practice_videos
  end
end
