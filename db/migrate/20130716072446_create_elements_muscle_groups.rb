class CreateElementsMuscleGroups < ActiveRecord::Migration
  def change
    create_table :elements_muscle_groups, :id => false do |t|
      t.integer :element_id
      t.integer :muscle_group_id
    end
  end
end
