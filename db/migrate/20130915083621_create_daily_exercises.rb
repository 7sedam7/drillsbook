class CreateDailyExercises < ActiveRecord::Migration
  def change
    create_table :daily_exercises do |t|
      t.integer :user_id
      t.integer :exercise_id
      t.datetime :datetime

      t.timestamps
    end
  end
end
