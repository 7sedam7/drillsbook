class RenameColumnNameesInExercises < ActiveRecord::Migration
  def self.up
    rename_column :exercises, :number_of_repetitions_in_series, :number_of_reps_in_set
    rename_column :exercises, :number_of_series, :number_of_sets
  end

  def self.down
    rename_column :exercises, :number_of_reps_in_set, :number_of_repetitions_in_series
    rename_column :exercises, :number_of_sets, :number_of_series
  end
end
