class AddCollumnApparatusIdToExercises < ActiveRecord::Migration
  def change
    add_column :exercises, :apparatus_id, :integer
  end
end
