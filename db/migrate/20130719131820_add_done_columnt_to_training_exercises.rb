class AddDoneColumntToTrainingExercises < ActiveRecord::Migration
  def change
    add_column :training_exercises, :done, :boolean
  end
end
