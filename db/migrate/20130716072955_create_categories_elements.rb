class CreateCategoriesElements < ActiveRecord::Migration
  def change
    create_table :categories_elements, :id => false do |t|
      t.integer :category_id
      t.integer :element_id
    end
  end
end
