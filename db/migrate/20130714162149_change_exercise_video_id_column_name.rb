class ChangeExerciseVideoIdColumnName < ActiveRecord::Migration
  def change
	rename_column :exercise_videos, :practice_id, :exercise_id
  end
end
