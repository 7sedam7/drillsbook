class CreatePractices < ActiveRecord::Migration
  def change
    create_table :practices do |t|
      t.integer :element_id
      t.integer :number_of_series
      t.integer :number_of_repetitions_in_series
      t.integer :load
      t.text :description

      t.timestamps
    end
  end
end
