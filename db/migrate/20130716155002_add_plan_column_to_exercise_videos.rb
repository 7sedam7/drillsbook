class AddPlanColumnToExerciseVideos < ActiveRecord::Migration
  def change
    add_column :exercise_videos, :plan, :boolean
  end
end
