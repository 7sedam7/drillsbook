class AddScopeIdToElements < ActiveRecord::Migration
  def change
    add_column :elements, :scope_id, :integer
  end
end
