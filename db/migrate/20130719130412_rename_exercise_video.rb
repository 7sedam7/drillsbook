class RenameExerciseVideo < ActiveRecord::Migration
  def self.up
    rename_table :exercise_videos, :training_exercises
  end

  def self.down
    rename_table :training_exercises, :exercise_videos
  end
end
