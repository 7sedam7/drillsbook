class RenamePractice < ActiveRecord::Migration
  def self.up
    rename_table :practices, :exercises
  end

  def self.down
    rename_table :exercises, :practices
  end
end
