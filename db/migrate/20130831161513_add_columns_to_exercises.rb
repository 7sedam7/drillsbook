class AddColumnsToExercises < ActiveRecord::Migration
  def change
    add_column :exercises, :hold, :boolean
    add_column :exercises, :length, :integer
  end
end
