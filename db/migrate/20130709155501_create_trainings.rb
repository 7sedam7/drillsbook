class CreateTrainings < ActiveRecord::Migration
  def change
    create_table :trainings do |t|
      t.datetime :start
      t.integer :length
      t.text :description

      t.timestamps
    end
  end
end
