# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Scope.create([
    { name: 'public' },
    { name: 'user_private' }
])

Role.create([
    { name: 'user' },
    { name: 'element_creator' }
])

Apparatus.create([
    { name: 'rings', short: 'XR' },
    { name: 'high bar', short: 'HB' },
    { name: 'floor', short: 'FX' },
    { name: 'stall bar', short: 'SB' },
    { name: 'parallel bars', short: 'PB' }
])
