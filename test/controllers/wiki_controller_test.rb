require 'test_helper'

class WikiControllerTest < ActionController::TestCase
  test "should get show_exercise" do
    get :show_exercise
    assert_response :success
  end

  test "should get show_element" do
    get :show_element
    assert_response :success
  end

end
