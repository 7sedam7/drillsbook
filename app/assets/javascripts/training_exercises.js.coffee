# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$ ->
    #$('ul').on('click', 'li div.training-exercise', ->
    $('.training-table').on('click', 'tbody tr', ->
        el = $(this)
        if el.hasClass('done')
            el.removeClass 'done'
            el.find('.done-cell i').removeClass 'icon-ok'
            el.find('input:text').val false
            el.find("input:submit").submit()
        else
            el.addClass 'done'
            el.find('.done-cell i').addClass 'icon-ok'
            el.find('input:text').val true
            el.find('input:submit').submit()
    )

    $('tr').popover()

    $('.te-len').hide()

    $('.te-cb').click ->
        el = $(this)
        if el.is(':checked')
            el.parent().find('.te-len').show()
            el.parent().find('.exerciseReps').hide()
        else
            el.parent().find('.te-len').hide()
            el.parent().find('.exerciseReps').show()
    
    $('.training-options').children().tooltip()

    $('.weekly').hide()
    $('.repeat-type').click ->
        option = $(this).val()
        if option == '2'
            $('.daily').hide()
            $('.weekly').show()
        else
            $('.daily').show()
            $('.weekly').hide()

    $('.startdatepicker').datetimepicker
        language: 'pt-BR'
        pickTime: false

    $('.enddatepicker').datetimepicker
        language: 'pt-BR'
        pickTime: false

    $('.after-rb').click ->
        $(this).find('input:radio').prop('checked', true)
        $(this).find('input:radio').prop('value', true)
        $('.on-rb').find('input:radio').prop('checked', false)
        $('.on-rb').find('input:radio').prop('value', false)

    $('.on-rb').click ->
        $(this).find('input:radio').prop('checked', true)
        $(this).find('input:radio').prop('value', true)
        $('.after-rb').find('input:radio').prop('checked', false)
        $('.after-rb').find('input:radio').prop('value', false)

    $('.repeat-submit').click ->
        $(this).parent().parent().find('.modal-body form').submit()

    $('.exerciseApparatus').each ->
        $(this).autocomplete
            source: $(this).data('autocomplete-source')
            messages: {
                noResult: '',
                results: ->
            }
