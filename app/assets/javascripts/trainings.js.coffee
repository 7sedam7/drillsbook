# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$ ->
    $("#datetimepicker1").datetimepicker
        language: "pt-BR"
        showTimeFirst: true

    exerciseObjs = {}
    #selectedElement = null

    $('.add_training_exercise').each ->
        #value = $(this).val()
        #alert 'value'
        selectedElement = $(this)
        $(this).typeahead
            source: (query, process) ->
                value = selectedElement.val()
                #alert value
                $.ajax
                    url: "/wiki/exercises.json?exercise=" + value
                    cache: false
                    success: (data) ->
                        exerciseObjs = {}
                        exerciseIds = []

                        $.each data, (i, item) ->
                            exerciseIds.push item.id
                            exerciseObjs[item.id] = item
                    
                        process exerciseIds

            updater: (selectedId) ->
                $(".exerciseId").val( exerciseObjs[selectedId].id )
                $('.exerciseDesc').val( exerciseObjs[selectedId].description )
                $('.exerciseApparatus').val( exerciseObjs[selectedId].apparatus )
                $('.exerciseLoad').val( exerciseObjs[selectedId].load )
                check = exerciseObjs[selectedId].hold
                if check
                    $('.exerciseReps').hide()
                    $('.te-len').show()
                $('.exerciseReps').val( exerciseObjs[selectedId].number_repetitions )
                $('.exerciseLength').val( exerciseObjs[selectedId].length )
                $('.te-cb').prop('checked', check )
                $('.exerciseSets').val( exerciseObjs[selectedId].number_of_sets )
                exerciseObjs[selectedId].element_name
            sorter: (items) ->
                items.sort (a, b) ->
                    return -1 if exerciseObjs[a].element_name < exerciseObjs[b].element_name
                    return 1 if exerciseObjs[a].element_name > exerciseObjs[b].element_name
                    0
            matcher: (item) ->
                true  unless exerciseObjs[item].element_name.toLowerCase().indexOf(@query.trim().toLowerCase()) is -1
            highlighter: (item) ->
                ex = exerciseObjs[item]
                itm = '<img src="' + ex.image_url + '">' +
                    "<strong>" + ex.element_name + ', ' + 
                    ex.apparatus + "</strong>" + " - " +
                      ex.load + "kg, "
                if ex.hold
                    itm += ex.length + 'sec'
                else
                    itm += ex.number_repetitions
                itm += "X" + ex.number_of_sets +
                      "<p>" + ex.description + "</p>"
                itm
