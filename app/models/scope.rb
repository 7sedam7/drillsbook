class Scope < ActiveRecord::Base
  has_many :elements

  validates :name, :uniqueness => true
end
