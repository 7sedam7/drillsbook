class User < ActiveRecord::Base
  before_validation :default_values
  has_many :trainings
  has_many :exercises
  has_many :elements
  has_many :daily_exercises, dependent: :destroy
  belongs_to :role

  validates :role, :presence => true

  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  def default_values
    self.role_id ||= Role.where(:name => 'user').first.id
  end
end
