require 'file_size_validator'
class Element < ActiveRecord::Base
    before_validation :default_values
    has_many :exercises
    has_and_belongs_to_many :categories
    has_and_belongs_to_many :muscle_groups
    belongs_to :user
    belongs_to :scope
    mount_uploader :image, ImageUploader
    validates :image,
        :presence => false,
        :file_size => { :maximum => 1.megabytes.to_i }

    def default_values
        self.scope_id ||= Scope.where(:name => 'user_private').first.id
        self.description ||= ''
    end
end
