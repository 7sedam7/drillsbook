class Exercise < ActiveRecord::Base
  before_validation :default_values
  has_many :training_exercises, dependent: :destroy
  has_many :daily_exercises, dependent: :destroy
  has_many :trainings, through: :training_exercises
  belongs_to :element
  belongs_to :user
  belongs_to :apparatus

  def self.get_or_create_by(user, element_name, apparatus_name, options = {})
    element = Element.find_or_create_by(name: element_name)
    apparatus = Apparatus.where(name: apparatus_name).first
    if options[:hold] == '1'
      options[:hold] = true
    elsif
      options[:hold] = false
    end
    options[:user_id] = user.id
    options[:element_id] = element.id
    options[:apparatus_id] = apparatus.id
 #   Exercise.where(options).first
    Exercise.find_or_create_by(options)
  end

  def element_name
    element.try(:name)
  end

  def element_name=(name)
    if name.strip == ''
        return nil
    end
    self.element = Element.find_or_create_by(:name => name) if name.present?
  end

  def apparatus_name
    apparatus.try(:name)
  end

  def apparatus_name=(name)
    self.apparatus = Apparatus.where(:name => name).first if name.present?
  end

  def default_values
      self.load ||= 0
      self.length ||= 0
      self.number_of_reps_in_set ||= 0
      self.number_of_sets ||= 0
      self.description ||= ''
      self.hold ||= false
      true
  end
end
