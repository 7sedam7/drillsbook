class Apparatus < ActiveRecord::Base
  has_many :exercises

  validates :name, :uniqueness => true
end
