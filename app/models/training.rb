class Training < ActiveRecord::Base
    has_many :training_types
    has_many :training_exercises, dependent: :destroy
    has_many :exercises, through: :training_exercises
    belongs_to :user
end
