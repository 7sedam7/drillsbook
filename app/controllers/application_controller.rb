class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def after_sign_in_path_for(resource_or_scope)
    user_training_path(current_user, Date.today)
  end

  def after_sign_up_path_for(resource)
    user_training_path(current_user, Date.today)
  end

  def after_inactive_sign_up_path_for(resource)
    root_pathh
  end
end
