class ExercisesController < ApplicationController
  def index
    @exercises = Exercise.where(:user_id => current_user.id)
    public_scope = Scope.where(:name => 'public').first
    e = Element.arel_table
    elements = Element.where(
        e[:scope_id].eq(public_scope.id).
        or(e[:user_id].eq(current_user.id))).where(
        e[:name].matches("%#{params[:exercise]}%"))
    respond_to do |format|
      format.html
      format.json do
          exercises_array = Array.new
          fake_id = -1
          elements.each do |el|
            has_exercises = false
            @exercises.where(:element_id => el.id).each do |ex|
              has_exercises = true
              exercises_array << create_exercise_hash({ex: ex, el: el})
            end
            unless has_exercises
                exercises_array << create_exercise_hash({ex: Exercise.new, el: el, id: fake_id})
                fake_id -= 1
            end
          end
          render json: exercises_array
      end
    end
  end

  def new
    @exercise = Exercise.new
  end

  def show
    @exercise = Exercise.find(params[:id])
    confirm_user @exercise
  end

  def create
    @exercise = Exercise.create(exercise_params)
    @exercise.element_name = params[:exercise][:element_name]
    if @exercise.element
      @exercise.element.user ||= current_user
      @exercise.element.save
    end
    @exercise.user = current_user
    if @exercise.save
      flash[:notice] = "Successfully created new exercise."
      redirect_to exercise_path(@exercise)
    else
      flash[:error] = @exercise.errors.full_messages
      render :action => 'new'
    end
  end

  def edit
    @exercise = Exercise.find(params[:id])
    confirm_user @exercise
  end

  def update
    @exercise = Exercise.find(params[:id])
    if @exercise.user != current_user
      flash[:error] = 'You can not edit this exercise'
      redirect_to exercises_path
    end
    if @exercise.update_attributes(exercise_params)
      @exercise.element.user ||= current_user
      @exercise.element.save
      flash[:notice] = 'Successfull update'
      redirect_to :action => 'show', :id => @exercise
    else
      render :action => 'edit'
    end
  end

  def destroy
    exercise = Exercise.find(params[:id])
    if exercise.user != current_user
      flash[:error] = 'You can not delete this exercise'
      return redirect_to exercises_path
    end
    if exercise.trainings.count > 0 or exercise.daily_exercises.count > 0
      redirect_to exercise_path(exercise), :flash => { :error => 'You can not delete this exercise because you have a training where you did it.' }
    else
      exercise.destroy
      redirect_to :action => 'index', :flash => { :notice => 'Exercise was successfully removed.' }
    end
  end

  private

  def create_exercise_hash(options={})
      default_string = ''
      default_number = 0
      ex = options[:ex]
      el = options[:el]
      apparatus = ex.apparatus || Apparatus.new
      {
          :id => ex.id || options[:id],
          :element_name => el.name || default_string,
          :apparatus => apparatus.name || default_string,
          :image_url => el.image_url(:thumb).to_s,
          :number_of_sets => ex.number_of_sets || default_number,
          :number_repetitions => ex.number_of_reps_in_set || default_number,
          :length => ex.length || default_number,
          :hold => ex.hold || false,
          :load => ex.load || default_number,
          :description => ex.description || default_string
      }
  end

  def confirm_user(exercise)
    if exercise.user != current_user
      flash[:error] = 'You do not have access to this exercise'
      redirect_to exercises_path
    end
  end

  def element_params
    params.require(:exercise).permit(:element_name)
  end

  def exercise_params
    params.require(:exercise).permit(:description, :load, :number_of_reps_in_set, :number_of_sets, :hold, :length)
  end
end
