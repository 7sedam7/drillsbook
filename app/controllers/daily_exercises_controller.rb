class DailyExercisesController < ApplicationController
  def index

  end

  def new

  end

  def create
    exercise = Exercise.get_or_create_by(current_user,
                                         params[:exercise][:name],
                                         params[:exercise][:apparatus],
                                         exercise_params)
#    exercise = Exercise.where(exercise_params).first_or_create
#    if exercise.element and exercise.element.name != params[:exercise][:name]
#        exercise = exercise.dup
#        exercise.element = nil
#    end
#    exercise.user ||= current_user
#    exercise.element_name = params[:exercise][:name]
#    exercise.apparatus_name = params[:exercise][:apparatus]
#    if exercise.element
#        exercise.element.user ||= current_user
#        exercise.element.save
#    end
#    exercise.save
    date = Date.parse(params[:date])
    time = Time.parse(params[:exercise][:time])
    datetime = date.to_datetime
    datetime = datetime.change({:hour => time.hour, :min => time.min, :sec => time.sec})
    @daily_exercise = DailyExercise.create(:exercise => exercise, :user => current_user, :datetime => datetime)
    respond_to do |format|
      format.js
    end
  end

  def destroy
    de = DailyExercise.find(params[:id])
    de.destroy
    respond_to do |format|
      format.js
    end
  end

  private

  def exercise_params
    params.require(:exercise).permit(:description, :number_of_sets, :number_of_reps_in_set, :load, :hold, :length)
  end
end
