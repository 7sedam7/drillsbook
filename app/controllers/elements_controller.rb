class ElementsController < ApplicationController
  def index
    public_scope = Scope.where(:name => 'public').first
    et = Element.arel_table
    @elements = Element.where(
        et[:scope_id].eq(public_scope.id).
        or(et[:user_id].eq(current_user.id))
    )
  end

  def show
    @element = Element.find(params[:id])
    confirm_user @element
  end

  def new
    @element = Element.new
  end

  def create
    @element = Element.create(element_params)
    @element.user = current_user
    if current_user.role == Role.where(name: 'element_creator').first
      @element.scope = Scope.where(name: 'public').first
    end
    if @element.save
      flash[:notice] = "Successfully created new element."
      redirect_to element_path(@element)
    else
      render :action => 'new'
    end
  end

  def edit
    @element = Element.find(params[:id])
    confirm_user @element
  end

  def update
    @element = Element.find(params[:id])
    if @element.user != current_user
        flash[:error] = 'You con not edit this element'
        redirect_to elements_path
    end
    if @element.update_attributes(element_params)
      flash[:notice] = 'Successfull update'
      redirect_to :action => 'show', :id => @element
    else
      render :action => 'edit'
    end
  end

  def destroy
    element = Element.find(params[:id])
    if element.user != current_user
      flash[:error] = 'You can not delete this element'
      redirect_to elements_path
    end
    if Exercise.where(:element_id => element.id).count > 0
      redirect_to element_path(element), :flash => { :error => 'You can not delete element used in existing exercise' }
    else
      element.destroy
      redirect_to elements_path, :flash => { :notice => 'Element successfully removed' }
    end
  end

  private

  def confirm_user(element)
    if element.user != current_user
      flash[:error] = 'You do not have access to this element'
      redirect_to elements_path
    end
  end

  def element_params
    params.require(:element).permit(:name, :description, :image, :remote_image_url)
  end
end
