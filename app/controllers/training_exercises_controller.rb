class TrainingExercisesController < ApplicationController
  before_filter :authenticate_user!

  def create
    exercise = Exercise.get_or_create_by(current_user, 
                                         params[:exercise][:name],
                                         params[:exercise][:apparatus],
                                         exercise_params)
#    exercise = Exercise.where(exercise_params).first_or_create
#    if exercise.element and exercise.element.name != params[:exercise][:name]
#        exercise = exercise.dup
#    end
#    exercise.user ||= current_user
#    exercise.element_name = params[:exercise][:name]
#    exercise.apparatus_name = params[:exercise][:apparatus]
#    if exercise.element
#      exercise.element.user ||= current_user
#      exercise.element.save
#    end
#    exercise.save
    @training = Training.find(params[:training_id])
    if @training.user == current_user
      @training_exercise = TrainingExercise.create(:exercise => exercise, :training => @training, :done => false, :plan => true)
    end
    respond_to do |format|
      format.js
    end
  end

  def update
    @training_exercise = TrainingExercise.find(params[:id])
    @training_exercise.update_attributes(training_exercise_params)
  end

  def destroy
    te = TrainingExercise.find(params[:id])
    @training = te.training
    te.destroy
    respond_to do |format|
      format.js
    end
  end

  private

  def training_exercise_params
    params.require(:training_exercise).permit(:done)
  end

  def exercise_params
    params.require(:exercise).permit(:description, :number_of_sets, :number_of_reps_in_set, :load, :hold, :length)
  end
end
