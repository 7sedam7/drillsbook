class TrainingsController < ApplicationController
  before_filter :authenticate_user!, :except => [:show]

  def create
    @training = Training.create(training_params)
    @training.user_id = current_user.id
    if @training.save
      flash[:notice] = 'Successfully created new training.'
      redirect_to user_training_path(current_user, @training.start.to_date)
    else
      redirect_to user_training_path(current_user, Date.parse(params[:id]))
    end
  end

  def index
    @date = params[:date] ? Date.parse(params[:date]) :  Date.today
    @datetime = DateTime.now.change({:year => @date.year, :month => @date.month, :day => @date.day, :hour => Time.now.hour, :min => Time.now.min})
    @trainings = current_user.trainings.where('start BETWEEN ? AND ?', Date.parse(params[:date]).beginning_of_day, Date.parse(params[:date]).end_of_day).order(:start)
    @training = Training.new
    @training_exercise = TrainingExercise.new
    @daily_exercises = current_user.daily_exercises.where('datetime BETWEEN ? AND ?', @date.beginning_of_day, @date.end_of_day)
    @daily_exercise = DailyExercise.new
  end

  def show
    begin
      @training = Training.find(params[:id])
    rescue
      @errors = ["There is no trainnig with id #{params[:id]}"]
    end
  end

  def destroy
    Training.find(params[:id]).destroy
  end

  private

  def training_params
    params.require(:training).permit(:description, :length, :start)
  end
end
