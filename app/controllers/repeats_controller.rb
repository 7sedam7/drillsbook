class RepeatsController < ApplicationController
  before_filter :authenticate_user!

  def create
    @training = Training.find(params[:repeat][:training_id])
    start_date = Date.parse(params[:repeat][:start_date])
    end_date = Date.parse(params[:repeat][:end_date])
    if params[:repeat][:type] == '1'
      offset = params[:repeat][:days].to_i
      offset = 0 if offset < 0
      if params[:repeat][:on]
        n = (end_date - start_date).to_i / offset
      else
        n = params[:repeat][:repeat_after].to_i
      end
      add_n_new_trainings @training, n, offset, start_date
    else
      offset = params[:repeat][:weeks].to_i
      offset = 0 if offset < 0
      start_date = start_date.next
      n = params[:repeat][:repeat_after].to_i
      if params[:repeat][:after]
        end_date = end_date + (n*offset).years
      else
        n = (end_date - start_date).to_i * offset
      end
      until start_date > end_date || n == 0
        if start_date.monday?
          add_new_training @training, start_date if params[:repeat][:monday] == '1'
        elsif start_date.tuesday?
          add_new_training @training, start_date if params[:repeat][:tuesday] == '1'
        elsif start_date.wednesday?
          add_new_training @training, start_date if params[:repeat][:wednesday] == '1'
        elsif start_date.thursday?
          add_new_training @training, start_date if params[:repeat][:thursday] == '1'
        elsif start_date.friday?
          add_new_training @training, start_date if params[:repeat][:friday] == '1'
        elsif start_date.saturday?
          add_new_training @training, start_date if params[:repeat][:saturday] == '1'
        elsif start_date.sunday?
          add_new_training @training, start_date if params[:repeat][:sunday] == '1'
          start_date = start_date + (7 * (offset-1)).days
          n -= 1
        end
        start_date = start_date.next
      end
    end
  end

  private

  def add_n_new_trainings(training, n, offset, date)
    until n == 0
      date += offset.to_i.days
      add_new_training training, date
      n -= 1
    end
  end

  def add_new_training(training, date)
    new_training = training.dup
    new_training.start = new_training.start.change({:month => date.month, :day => date.day})
    new_training.save
    TrainingExercise.where(:training_id => training.id).each do |te|
      new_te = te.dup
      new_te.training_id = new_training.id
      new_te.done = false
      new_te.save
    end
  end

  def repeat_params
    params.require(:repeat).permit()
  end
end
