DrillsBook::Application.routes.draw do

  get "repeats/create"
  get "home/index"
  devise_for :users, :controllers => {:registrations => "registrations"}

  get 'users/:user_id/trainings/:date', to: 'trainings#index', date: /\d{4}-\d{2}-\d{2}/
  get 'trainings/:id', to: 'trainings#show', as: 'training', id: /\d+/

  resources :repeats, only: :create
  resources :daily_exercises, except: [:show, :edit, :update]

  resources :users do
    resources :trainings, except: :show
  end
  resources :training_exercises
  resources :exercises, path: 'wiki/exercises'
  resources :elements, path: 'wiki/elements'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'home#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end
  
  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
